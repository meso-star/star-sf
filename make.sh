#!/bin/sh

# Copyright (C) 2016-2018, 2021-2023 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

################################################################################
# Helper functions
################################################################################
# Print the value of a variable in config.mk
showvar()
{
  make="$1"
  # To avoid messages from Make being displayed instead of the value of the
  # queried variable, we redirect its output to /dev/null and open a new file
  # descriptor to stdout to print the variable
<< EOF "${make}" -f 3>&1 1>/dev/null 2>&1 - || kill -HUP $$
.POSIX:
include config.mk
showvar:
	@1>&3 echo \$($2)
EOF
  exec 3<&- # Close file descriptor 3
}

check_cpuflag()
{
  sed -n "/^flags[[:blank:]]\{1,\}:/{p;q}" /proc/cpuinfo \
| sed "s/.*[[:blank:]]\{1,\}\($1\)[[:blank:]]\{1,\}.*/\1/"
}

################################################################################
# Main functions
################################################################################
config_simd()
{
  make="$1"
  simd_width="$(showvar "${make}" SIMD_WIDTH)"
  pkg_config="$(showvar "${make}" PKG_CONFIG)"
  avx="$(check_cpuflag avx)"
  if [ -z "${simd_width}" ]; then
    if ! ${pkg_config} --exists rsimd; then
      simd_width="NONE"
    elif [ -n "${avx}" ]; then
      simd_width="256"
    else
      simd_width="128"
    fi
  fi
  printf "SIMD_WIDTH = %s\n" "${simd_width}"
}

config_test()
{
  for i in "$@"; do
    test=$(basename "${i}" ".c")
    test_list="${test_list} ${test}"
    printf "%s: src/%s.o\n" "${test}" "${test}"
  done
  printf "test_bin: %s\n" "${test_list}"
}

check()
{
  name="$1"
  prog="$2"
  shift 2

  printf "%s " "${name}"
  if ./"${prog}" "$@" > /dev/null 2>&1; then
    printf "\033[1;32mOK\033[m\n"
  else
    printf "\033[1;31mError\033[m\n"
  fi 2> /dev/null
}

run_test()
{
  for i in "$@"; do
    test=$(basename "${i}" ".c")
    if [ "${test}" != "test_ssf_phase_rdgfa" ]; then
      check "${test}" "${test}"
    else
      macro="SIMD_WIDTH"
      value="[^[:blank:]]\{0,\}"
      spaces="[[:space:]]\{0,\}"
      simd_width=$(sed "s/${macro}${spaces}=${spaces}\(${value}\)${spaces}$/\1/" .simd)
      if [ "${simd_width}" = "256" ]; then
        check "${test}_simd_256" "${test}" simd_256
      fi
      if [ "${simd_width}" = "256" ] || [ "${simd_width}" = 128 ]; then
        check "${test}_simd_128" "${test}" simd_128
      fi
      check "${test}_simd_none" "${test}" simd_none
    fi
  done 2> /dev/null
}

clean_test()
{
  for i in "$@"; do
    rm -f "$(basename "${i}" ".c")"
  done
}

install()
{
  prefix=$1
  shift 1

  for i in "$@"; do
    # Remove the "src" directory and append the "prefix"
    dst="${prefix}/${i#*/}"

    # Create the Install directory if required
    dir="${dst%/*}"
    if [ ! -d "${dir}" ]; then
      mkdir -p "${dir}"
    fi

    if cmp -s "${i}" "${dst}"; then
      printf "Up to date %s\n" "${dst}"
    else
      printf "Installing %s\n" "${dst}"
      cp "${i}" "${dst}"
    fi
  done
}

"$@"
