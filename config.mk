VERSION = 0.9.0
PREFIX = /usr/local

LIB_TYPE = SHARED
#LIB_TYPE = STATIC

BUILD_TYPE = RELEASE
#BUILD_TYPE = DEBUG

# If not set, SIMD WIDTH is retrieved from host CPU
#SIMD_WIDTH = NONE
#SIMD_WIDTH = 128
#SIMD_WIDTH = 256

################################################################################
# Tools
################################################################################
AR = ar
CC = cc
LD = ld
OBJCOPY = objcopy
PKG_CONFIG = pkg-config
RANLIB = ranlib

################################################################################
# Dependencies
################################################################################
PCFLAGS_STATIC = --static
PCFLAGS = $(PCFLAGS_$(LIB_TYPE))

RSIMD_VERSION = 0.5
RSIMD_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags rsimd)
RSIMD_CFLAGS_128 = $(RSIMD_CFLAGS)
RSIMD_CFLAGS_256 = $(RSIMD_CFLAGS)
RSIMD_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs rsimd)
RSIMD_LIBS_128 = $(RSIMD_LIBS)
RSIMD_LIBS_256 = $(RSIMD_LIBS)

RSYS_VERSION = 0.14
RSYS_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags rsys)
RSYS_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs rsys)

SSP_VERSION = 0.14
SSP_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags star-sp)
SSP_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs star-sp)

DPDC_CFLAGS_SIMD_128 = $(RSIMD_CFLAGS)
DPDC_CFLAGS_SIMD_256 = $(RSIMD_CFLAGS)
DPDC_LIBS_SIMD_128 = $(RSIMD_LIBS)
DPDC_LIBS_SIMD_256 = $(RSIMD_LIBS)

DPDC_CFLAGS = $(RSIMD_CFLAGS_$(SIMD_WIDTH)) $(RSYS_CFLAGS) $(SSP_CFLAGS)
DPDC_LIBS = $(RSIMD_LIBS_$(SIMD_WIDTH)) $(RSYS_LIBS) $(SSP_LIBS) -lm

################################################################################
# Compilation options
################################################################################
WFLAGS =\
 -Wall\
 -Wcast-align\
 -Wconversion\
 -Wextra\
 -Wmissing-declarations\
 -Wmissing-prototypes\
 -Wshadow

CFLAGS_SIMD_128 = -DSSF_USE_SIMD_128
CFLAGS_SIMD_256 = $(CFLAGS_SIMD_128) -DSSF_USE_SIMD_256
CFLAGS_SIMD = $(CFLAGS_SIMD_$(SIMD_WIDTH))

CFLAGS_HARDENED =\
 -D_FORTIFY_SOURCES=2\
 -fcf-protection=full\
 -fstack-clash-protection\
 -fstack-protector-strong

CFLAGS_COMMON =\
 -std=c89\
 -pedantic\
 -fvisibility=hidden\
 -fstrict-aliasing\
 $(CFLAGS_HARDENED)\
 $(WFLAGS)\

CFLAGS_DEBUG = -g $(CFLAGS_COMMON)
CFLAGS_RELEASE = -O2 -DNDEBUG $(CFLAGS_COMMON)
CFLAGS = $(CFLAGS_$(BUILD_TYPE))

CFLAGS_SO = $(CFLAGS) -fPIC
CFLAGS_EXE = $(CFLAGS) -fPIE

################################################################################
# Linker options
################################################################################
LDFLAGS_HARDENED = -Wl,-z,relro,-z,now
LDFLAGS_DEBUG = $(LDFLAGS_HARDENED)
LDFLAGS_RELEASE = -s $(LDFLAGS_HARDENED)
LDFLAGS = $(LDFLAGS_$(BUILD_TYPE))

LDFLAGS_SO = $(LDFLAGS) -shared -Wl,--no-undefined
LDFLAGS_EXE = $(LDFLAGS) -pie

OCPFLAGS_DEBUG = --localize-hidden
OCPFLAGS_RELEASE = --localize-hidden --strip-unneeded
OCPFLAGS = $(OCPFLAGS_$(BUILD_TYPE))
