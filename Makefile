# Copyright (C) 2016-2018, 2021-2023 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libssf.a
LIBNAME_SHARED = libssf.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Star-3D building
################################################################################
SRC =\
 src/ssf.c\
 src/ssf_beckmann_distribution.c\
 src/ssf_blinn_distribution.c\
 src/ssf_bsdf.c\
 src/ssf_fresnel.c\
 src/ssf_fresnel_constant.c\
 src/ssf_fresnel_dielectric_conductor.c\
 src/ssf_fresnel_dielectric_dielectric.c\
 src/ssf_fresnel_no_op.c\
 src/ssf_lambertian_reflection.c\
 src/ssf_microfacet_distribution.c\
 src/ssf_microfacet_reflection.c\
 src/ssf_phase.c\
 src/ssf_phase_discrete.c\
 src/ssf_phase_hg.c\
 src/ssf_phase_rayleigh.c\
 src/ssf_phase_rdgfa.c\
 src/ssf_pillbox_distribution.c\
 src/ssf_specular_dielectric_dielectric_interface.c\
 src/ssf_specular_reflection.c\
 src/ssf_thin_specular_dielectric.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library:

build_library__: .config $(DEP)
	@$(MAKE) -f.simd -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then\
	     echo "$(LIBNAME)";\
	   else\
	     echo "$(LIBNAME_SHARED)";\
	   fi)

$(DEP) $(OBJ): config.mk .simd

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) $(CFLAGS_SIMD) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(DPDC_LIBS)

$(LIBNAME_STATIC): libssf.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libssf.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk .simd
	@if [ "$(SIMD_WIDTH)" = "128" ] || [ "$(SIMD_WIDTH)" = "256" ]; then \
	   if ! $(PKG_CONFIG) --atleast-version $(RSIMD_VERSION) rsimd; then \
	     echo "rsimd $(RSIMD_VERSION) not found"; exit 1; fi; fi
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found"; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SSP_VERSION) star-sp; then \
	  echo "star-sp $(SSP_VERSION) not found"; exit 1; fi
	@echo "config done" > $@

.simd: make.sh config.mk
	@$(SHELL) make.sh config_simd $(MAKE) > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS_SO) $(CFLAGS_SIMD) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) $(CFLAGS_SIMD) $(DPDC_CFLAGS) -DSSF_SHARED_BUILD -c $< -o $@

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(LIBNAME)
	rm -f .config .simd .test libssf.o ssf.pc ssf-local.pc

distclean: clean
	rm -f $(DEP) $(TEST_DEP)

lint:
	shellcheck -o all make.sh

build_library build_tests pkg: .simd
	@$(MAKE) -f.simd -fMakefile $@__

################################################################################
# Installation
################################################################################
PKG_RSIMD = rsimd >= $(RSIMD_VERSION),
PKG_SIMD_128 = $(PKG_RSIMD)
PKG_SIMD_256 = $(PKG_RSIMD)
PKG_SIMD = $(PKG_SIMD_$(SIMD_WIDTH))

pkg__:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@SSP_VERSION@#$(SSP_VERSION)#g'\
	    -e 's#@SIMD@#$(PKG_SIMD)#g'\
	    ssf.pc.in > ssf.pc

ssf-local.pc: ssf.pc.in .simd
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@SSP_VERSION@#$(SSP_VERSION)#g'\
	    -e 's#@SIMD@#$(PKG_SIMD)#g'\
	    ssf.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" ssf.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" src/ssf.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/star-sf" COPYING README.md

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/ssf.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-sf/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-sf/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/ssf.h"

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_ssf_beckmann_distribution.c\
 src/test_ssf_blinn_distribution.c\
 src/test_ssf_bsdf.c\
 src/test_ssf_fresnel.c\
 src/test_ssf_fresnel_constant.c\
 src/test_ssf_fresnel_dielectric_conductor.c\
 src/test_ssf_fresnel_dielectric_dielectric.c\
 src/test_ssf_fresnel_no_op.c\
 src/test_ssf_lambertian_reflection.c\
 src/test_ssf_microfacet_distribution.c\
 src/test_ssf_microfacet_reflection.c\
 src/test_ssf_phase.c\
 src/test_ssf_phase_discrete.c\
 src/test_ssf_phase_hg.c\
 src/test_ssf_phase_rayleigh.c\
 src/test_ssf_phase_rdgfa.c\
 src/test_ssf_pillbox_distribution.c\
 src/test_ssf_specular_dielectric_dielectric_reflection.c\
 src/test_ssf_specular_reflection.c\
 src/test_ssf_thin_specular_dielectric.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
SSF_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags ssf-local.pc)
SSF_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs ssf-local.pc)

build_tests__: build_library $(TEST_DEP) .test ssf-local.pc
	@$(MAKE) -fMakefile -f.test $$(for i in $(TEST_DEP); do echo -f"$${i}"; done) test_bin

test: build_tests .simd
	@$(SHELL) make.sh run_test $(TEST_SRC)

.test: Makefile
	@$(SHELL) make.sh config_test $(TEST_SRC) > .test

clean_test:
	@$(SHELL) make.sh clean_test $(TEST_SRC)

$(TEST_DEP): config.mk ssf-local.pc
	@$(CC) $(CFLAGS_EXE) $(SSF_CFLAGS) $(RSYS_CFLAGS) $(SSP_CFLAGS) -MM -MT \
	"$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk ssf-local.pc
	$(CC) $(CFLAGS_EXE) $(SSF_CFLAGS) $(RSYS_CFLAGS) $(SSP_CFLAGS) -c $(@:.o=.c) -o $@

test_ssf_beckmann_distribution \
test_ssf_blinn_distribution \
test_ssf_bsdf \
test_ssf_fresnel \
test_ssf_fresnel_constant \
test_ssf_fresnel_dielectric_conductor \
test_ssf_fresnel_dielectric_dielectric \
test_ssf_fresnel_no_op \
test_ssf_lambertian_reflection \
test_ssf_microfacet_distribution \
test_ssf_microfacet_reflection \
test_ssf_phase \
test_ssf_phase_discrete \
test_ssf_phase_hg \
test_ssf_phase_rayleigh \
test_ssf_phase_rdgfa \
test_ssf_pillbox_distribution \
test_ssf_specular_dielectric_dielectric_reflection \
test_ssf_specular_reflection \
test_ssf_thin_specular_dielectric \
: config.mk ssf-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(SSF_LIBS) $(RSYS_LIBS) $(SSP_LIBS) -lm
